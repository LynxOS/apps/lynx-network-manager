async function drawNetworks(){
    openLoading();
    let html = '<ul class="list-group">'
    const networks = await network.getWifiList(true);

    for (net of networks){
        let id = net.BSSID.replaceAll(":", "-")
        html = html.concat(`
            <li class="list-group-item ${isActiveCSSStyle(net.inUseBoolean)}">
                <div class="container"><div class="row">
                    <div class="col-9">
                        ${net.SSID} <br />
                        ${drawPasswordInput(net.inUseBoolean, id, (net.SECURITY !== "--"))}
                    </div>
                    <div class="col-3 text-center">
                        <img src="file://${setSignalIcon(parseInt(net.SIGNAL))}" class="img-fluid" /><br />
                        ${drawCallToActionButtom(net.inUseBoolean, net.SSID, id, (net.SECURITY !== "--"))}
                    </div>
                </div></div>
            </li>
        `);
    }

    document.getElementById("networks").innerHTML = html.concat('</ul>');
    closeLoading()
}

function isActiveCSSStyle(isActive){
    let status = ""
    if (isActive){
        status="active"
    }

    return status
}

function drawCallToActionButtom(isConnected, ssid, id, havePass){
    let button = "";
    if (!isConnected){
        button = `
            <button class="btn btn-primary" onclick='connect("${ssid}","${id}", ${havePass})'>
                Connect
            </button>
        `;
    }
    return button
}

function drawPasswordInput(isConnected, id, havePass){
    let input =""
    if (!isConnected && havePass){
        input = `
            <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="Password" aria-label="Password" id="${id}">
            </div>
        `;
    }
    return input;
}

function setSignalIcon(signal){
    let icon;

    if (signal > 90){
        icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'network-wireless-signal-excellent-symbolic']).stdout.toString();
    } else if (signal > 80) {
        icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'network-wireless-signal-good-symbolic']).stdout.toString();
    } else if (signal > 50 ) {
        icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'network-wireless-signal-ok-symbolic']).stdout.toString();
    } else if (signal > 30) {
        icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'network-wireless-signal-weak-symbolic']).stdout.toString();
    } else {
        icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'network-wireless-signal-none-symbolic']).stdout.toString();
    }

    return icon;
}

async function drawWiFIStatus(){
    await setSwitch();
    document.getElementById("wifistatus").innerHTML = `
        <img src="file://${getIconWiFiStatus()}" class="img-fluid" />
    `;
}

function getIconWiFiStatus(){
    let icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'network-wireless-disabled-symbolic']).stdout.toString();
    if (document.getElementById("wifiSwitch").checked){
        icon = execSynx('python', ['/usr/share/Lynx/lynx-desktop-service/Lynx/getIcon.py', 'network-wireless-connected-symbolic']).stdout.toString();
    }
    return icon;
}

async function setSwitch(){
    let status = await network.getWifiStatus();
    if (status === "enabled"){
        document.getElementById("wifiSwitch").checked = true;
    } else {
        document.getElementById("wifiSwitch").checked = false;
    }
}

drawNetworks();
drawWiFIStatus();