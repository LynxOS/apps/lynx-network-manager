function connect(ssid, id, havePass){
    if (havePass){
        let pass = document.getElementById(id).value;
        network.wifiConnect(ssid, pass);
    }else{
        network.wifiConnect(ssid);
    }
}

async function switchWiFiConnection(){
    openLoading();
    const isActive = ! document.getElementById("wifiSwitch").checked;

    if (isActive){
        await network.wifiDisable();
        await new Promise(r => setTimeout(r, 2000));
    }else {
        await network.wifiEnable();
        await new Promise(r => setTimeout(r, 2000));
    }

    drawNetworks();
    drawWiFIStatus();
}
